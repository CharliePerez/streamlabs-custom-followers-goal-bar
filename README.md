# Current Version: 1.0.0

This code was written by me, and created for a custom "Followers Goal" bar to be used during my streams on [Streamlabs OBS](https://streamlabs.com/) if you have any doubt feel free to send me a meesage on any of my social accounts. If you want to add something, feel free to contribute.

[Instagram](https://www.instagram.com/heroiccharly/)

[Twitter](https://twitter.com/HeroicCharly)

Go show some support by following me on my Twitch channel [Heroic Charly](https://www.twitch.tv/heroiccharly)

## Example on CodePen.io

https://codepen.io/carlos-ernesto/pen/xxOaXVp

I would recommend to use codepen to give shape and style to your bar and at the end just copy and paste it to streamlabs.

# REQUIREMENTS

In order to modify this bar you'll need to have basic understanding of the next topics.

- HTML
- CSS
- JQuery

# CUSTOM HTML CSS TABS EXPLANAITION

Any changes you do in the different tabs will reload the preview of your bar in the upper section. Keep that in mind when you do changes.

## HTML

In here you can start giving shape to you bar by using only HTML.

## CSS

Here you will be able to give the look you want to you bar by applying different styles and attributes to your HTML elements.

## JS

Exploring the streamlabs website I noticed that they are setting some properties to the default bar by using JQuery. I decided to keep the same since I was not planning to add anything different at the moment. Down is the default code used in that section, if you are already familiar with JQuery you'll notice they are just getting the values and assigning them to the different HTML.

Note: Since this is the default code, this won't match with the current code in this project.

```
// Events will be sent when someone followers
// Please use event listeners to run functions.

document.addEventListener('goalLoad', function(obj) {
  // obj.detail will contain information about the current goal
  // this will fire only once when the widget loads
  console.log(obj.detail);
  $('#title').html(obj.detail.title);
  $('#goal-current').text(obj.detail.amount.current);
  $('#goal-total').text(obj.detail.amount.target);
  $('#goal-end-date').text(obj.detail.to_go.ends_at);
});

document.addEventListener('goalEvent', function(obj) {
  // obj.detail will contain information about the goal
  console.log(obj.detail);
  $('#goal-current').text(obj.detail.amount.current);
});
```

# HOW TO USE IT

## Fresh Start

I will supose that you already set up your first scene and you already know how to add the basic sources to your scene. With that being said...

1.- On your Sources Section click on the '+' sign and add a "Follower Goal"

![sources](Assets/sources.jpg)

![follower-goal](Assets/follower-goal.jpg)

2.- Double click on "Follower Goal" or right click on "Follower Goal" and click on "Properties"

## I already have a Follower Bar on my scene

First let's open the settings for the followers goal and click on "HTML CSS"

![HTMLCSS](Assets/customHTML.jpg)

Once you already clicked on HTML CSS, click on the "Enable Custom Code", after that on the lower part of the window you'll see these 3 sections. One for HTML, one for CSS and another one for JS (JavaScript).

If you already have the basic knoledge of the required topics, then you can start writing the HTML and CSS to start giving form and shape to your bar. The preview will refresh everytime you make changes that's useful when you start adding the JS since you want to reflect your active goal, notice in the image below that it's already pulling my current followers (38) and my current goal (50)

![Code Sections](Assets/code.jpg)
