// Events will be sent when someone follows you.
// Please use event listeners to run functions.

document.addEventListener('goalLoad', function (obj) {
  // obj.detail will contain information about the current goal
  // this will fire only once when the widget loads
  var progress =
    (Number(obj.detail.amount.current) * 100) / Number(obj.detail.amount.target);
  var value = '';
  $('#current-progress').width(value.concat(progress, '%'));
  $('#title').html(obj.detail.title);
  $('#goal-current').text(obj.detail.amount.current);
  $('#goal-total').text(obj.detail.amount.target);
  $('#goal-end-date').text(obj.detail.to_go.ends_at);
});

document.addEventListener('goalEvent', function (obj) {
  // obj.detail will contain information about the goal
  var progress =
    (Number(obj.detail.amount.current) * 100) / Number(obj.detail.amount.target);
  var value = '';
  $('#current-progress').width(value.concat(progress, '%'));
  $('#goal-current').text(obj.detail.amount.current);
});
